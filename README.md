# Object Oriented Java Projects From College: Alex's student repository

This repository holds Java code that was created for my classes from CCAC. (FALL 2021)
It's been a while since this class, so I don't remember the actual assignment names.

# CIT-130

CIT-130 is a very basic prgramming class. I believe it was my second Java clas at CCAC.
All assignments and tests covered here will be from this class. 

# Assignemnt 1: Operations

This assignment uses user input for addition, subtraction, multiplication, division, and modulus.

# Assignemnt 2: Operations

This assignment is similar to the last assignemnt, but the way user input gathered is changed.

# Assignemnt 3: Methods

From the looks of it, this assignments focus was methods in Java.
There are two files provided, run the demo file.

# Assignemnt 4: Working With Files

Program is given a file with phone number called phonenumbers.txt. The program determines if the program is valid or invalid.

# Assignemnt 5: Working With Methods and @Override

This assignment contains 3 files. You would run the RoboJob_Demo.java file. The user is given 6 choices and they must make a selection. Then the program performs the selected job. 

# Assignemnt 6: Wokring With GUI's

Assignment 6 creates and uses a GUI. The professor encouraged creative freedom with this assignment.
I made my GUI on the show It's Always Sunny in Philadelphia. All you need to to is click on the buttons/pictures.
There are some missing pictures, since I created this in 2021 and some links have broken. It still works though.

# Test 1: Arrays

This test had us create three programs. All three are similar and in each on an array of 5 numbers is created by user input. Then depening on which program is running, it dtermines how many even numbers there are.

# Test 2: Methods Arrays, overloaded constructors, objects, and more

Professor gave me 2 files of prewritten code. He then had four commented spots on wach file which we had to write our own code.

# Test 3: OOP, inheritance, and more

Test 3 has five total files. You would run the TVShow-Demo.java file. 
