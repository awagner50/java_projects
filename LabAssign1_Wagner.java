// CIT130 - Lab Assignment 1
// Alex Wagner 09/19/21

package labassignments;

import java.util.Scanner;

public class LabAssign1_Wagner {

    public static void main(String[] args) {
       
        int firstNum, secondNum, operation;
        double secondNumDouble;
        Scanner input = new Scanner(System.in);
        
        System.out.print("Enter first integer: -> ");
        firstNum = input.nextInt();
        System.out.print("Enter second integer: -> ");
        secondNum = input.nextInt();
        System.out.print("Enter operation choice"
                + "\n1 - addition"
                + "\n2 - subtraction"
                + "\n3 - multiplication"
                + "\n4 - division"
                + "\n5 - modulus"
                + "\nchoice: --> ");
        operation = input.nextInt();
        
        switch (operation) {           
            case 1: 
                System.out.println("\nSum is " + (firstNum + secondNum)); 
                break;
            case 2: 
                System.out.println("\nDifference is " + (firstNum - secondNum)); 
                break;
            case 3: 
                System.out.println("\nProduct is " + (firstNum * secondNum)); 
                break;
            case 4:
                switch (secondNum) {
                    case 0:
                        System.out.println("\nDivision by zero is not allowed."); 
                        break;
                    default:
                        secondNumDouble = secondNum / 1.0;
                        System.out.println("\nQuotient is " + (firstNum / secondNumDouble));
                }
                break;
            case 5: 
                switch (secondNum) {
                    case 0:
                        System.out.println("\nRemainder division by zero is not allowed."); 
                        break;
                    default:
                        System.out.println("\nRemainder division is " 
                                        + (firstNum % secondNum)); 
                }  
                break;
            default: 
                System.out.println("\nInvalid Operation entered.");
        }
    }  
}
