// CIT130 - Lab Assignment 4
// Alex Wagner 11/14/21

package labassignments;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class LabAssign4_Wagner {

    public static void main(String[] args) {
        
        int num1, num2, num3;
        int count = 0;
        String  line;
        String[] tokens;
        Scanner scan = null;
        File file = new File("phonenumbers.txt");
        
        try {
            scan = new Scanner(file);
        }
        catch(FileNotFoundException fnfe) {
            System.out.println("Error reading from the text file!");
            System.exit(0);
        }
        
        while(scan.hasNext()) {
            line = scan.nextLine();
            tokens = line.split("-");
            
            if(tokens.length == 3 && tokens[0].length() == 3 
                    && tokens[1].length() == 3 && tokens[2].length() == 4) {
                try {
                   num1 =  Integer.parseInt(tokens[0]);
                   num2 =  Integer.parseInt(tokens[1]);
                   num3 =  Integer.parseInt(tokens[2]);
                   
                   System.out.println(num1 + "-" + num2 + "-" + num3);
                   count ++;
                }
                catch(NumberFormatException nfe) {
                    System.out.println("Some or all data in this number are"
                            + " not ints: " + tokens[0] + "-" + tokens[1]
                            + "-" + tokens[2]);
                }
            }
            else if(tokens.length == 1) {
                System.out.println("Invalid Phone Number Format: " 
                        + tokens[0]);
            }
            else if (tokens.length == 2)  {
                System.out.println("Invalid Phone Number Format: " 
                        + tokens[0] + "-" + tokens[1]);
            }    
            else if (tokens.length == 3)  {
                System.out.println("Invalid Phone Number Format: " 
                        + tokens[0] + "-" + tokens[1] + "-" + tokens[2]);
            }
            else{
                System.out.println("Invalid Phone Number Format: " 
                        + tokens[0] + "-" + tokens[1] + "-" + tokens[2] 
                        + "-...");
                }
        }
        
        scan.close();
        System.out.println("\n" + count + " of the phone numbers are valid.");
    }
    
}
