// CIT130 - Lab Assignment 2
// Alex Wagner 09/26/21

package labassignments;

import java.util.Scanner;

public class LabAssign2_Wagner {

    public static void main(String[] args) {
        
        int firstNum, secondNum;
        double secondNumDouble;
        char operant;
        boolean runOne = true;
        Scanner input = new Scanner(System.in);
         
        firstNum = getOperand(input, "first");
        secondNum = getOperand(input, "second");
        
        do {
            if (runOne != true) {
                System.out.println("\nInvalid Operation entered.");
            }
            
            System.out.print("Enter operation choice"
                    + "\n+ for addition"
                    + "\n- for subtraction"
                    + "\n* for multiplication"
                    + "\n/ for division"
                    + "\n% for modulus"
                    + "\nchoice: --> ");
            String operation = input.next();
            operant = operation.charAt(0);
            runOne = false;
        }while(operant != '+'
                && operant != '-' 
                && operant != '*' 
                && operant != '/' 
                && operant != '%');
        
        switch (operant) {           
            case '+': 
                System.out.println("\nSum is " + (firstNum + secondNum)); 
                break;
            case '-': 
                System.out.println("\nDifference is " + (firstNum - secondNum)); 
                break;
            case '*': 
                System.out.println("\nProduct is " + (firstNum * secondNum)); 
                break;
            case '/':
                switch (secondNum) {
                    case 0:
                        System.out.println("\nDivision by zero is not allowed."); 
                        break;
                    default:
                        secondNumDouble = secondNum / 1.0;
                        System.out.println("\nQuotient is " + (firstNum / secondNumDouble));
                }
                break;
            case '%': 
                switch (secondNum) {
                    case 0:
                        System.out.println("\nRemainder division by zero is not allowed."); 
                        break;
                    default:
                        System.out.println("\nRemainder division is " 
                                        + (firstNum % secondNum)); 
                }  
        }        
    }

    public static int getOperand(Scanner scan, String wordNumber) {
        
        System.out.print("Enter " + wordNumber + " integer: -> ");
        int operand = scan.nextInt();

        return operand;
    }    
}
