// CIT130 - Lab Assignment 6
// Alex Wagner 12/12/21

package labassignments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LabAssign6_Wagner extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        
        //Panes
        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(20, 20, 20, 20));
        
        //Images
        Image Charlie = new Image("https://c8d8q6i8.stackpathcdn.com/wp-content/uploads/2018/03/Charlie-Day-Contact-Information.jpg", 200, 200, false, false);
        Image Frank = new Image("https://www.fxnowcanada.ca/wp-content/uploads/2014/03/Sunny_in_Philly_Danny-300x300.jpg", 200, 200, false, false);
        Image Mac = new Image("https://bleedingcool.com/wp-content/uploads/2020/03/rob-mcelhenney-900x900.jpg", 200, 200, false, false);
        Image Dennis = new Image("https://akns-images.eonline.com/eol_images/Entire_Site/201377/rs_600x600-130807125715-300.GlennHowerton.jc.713.jpg?fit=around%7C1080:1080&output-quality=90&crop=1080:1080;center,top", 200, 200, false, false);
        Image Dee = new Image("https://styles.redditmedia.com/t5_3aowlg/styles/communityIcon_qnmn9c8r6ru51.png", 200, 200, false, false);
        Image IASIP = new Image("https://media.customon.com/unsafe/600x600/img.customon.com//art/2/600/600/ffffff-5c488b/46011/3a73f742dce90ab8ed0e41aa6724c8be.png.jpg", 200, 200, false, false);
        
        //HBox and VBox
        VBox vb = new VBox();
        HBox hb1 = new HBox();
        HBox hb2 = new HBox();
        
        //Buttons
        Button btnCharlie = new Button();
        Button btnFrank = new Button();
        Button btnMac = new Button();
        Button btnDennis = new Button();
        Button btnDee = new Button();
        Button btnIASIP = new Button();
        
        //Set Nodes to Panes
        pane.setTop(vb);
        pane.setCenter(hb1);
        pane.setBottom(hb2);
        
        //vb - Top Pane
        Text title = new Text("Click Your Favorite Cast Member!");
        title.setFont(Font.font("Lucida", 25));
        
        Text names = new Text("The characters name in \"It's Always Sunny In Philadelphia\""
                + " will show up below.");
        names.setFont(Font.font("Lucida", 12));
        
        TextField tf = new TextField("");
        tf.setEditable(false);
        
        vb.setAlignment(Pos.CENTER);
        vb.setSpacing(12);
        vb.getChildren().addAll(title, names, tf);
        
        //hb1 - Middle Pane
        btnDennis.setGraphic(new ImageView(Dennis));
        btnDee.setGraphic(new ImageView(Dee));
        btnIASIP.setGraphic(new ImageView(IASIP));
        
        hb1.setAlignment(Pos.CENTER);
        hb1.setSpacing(20);
        hb1.getChildren().addAll(btnDennis, btnIASIP, btnDee);
        
        //hb2 - Bottom Pane
        btnCharlie.setGraphic(new ImageView(Charlie));
        btnMac.setGraphic(new ImageView(Mac));
        btnFrank.setGraphic(new ImageView(Frank));
        
        hb2.setAlignment(Pos.CENTER);
        hb2.setSpacing(20);
        hb2.getChildren().addAll(btnCharlie, btnMac, btnFrank);
        
        //Functionality
        btnCharlie.setOnAction((ActionEvent event) -> {
            onButtonPress(tf, "Charlie Kelly", "Charlie is portrayed by Charlie Day.");
        });
        btnFrank.setOnAction((ActionEvent event) -> {
            onButtonPress(tf, "Frank Reynolds", "Frank is portrayed by Danny DeVito.");
        });
        btnMac.setOnAction((ActionEvent event) -> {
            onButtonPress(tf, "Ronald \"Mac\" McDonald", "Mac is portrayed by Rob McElhenney.");
        });
        btnDennis.setOnAction((ActionEvent event) -> {
            onButtonPress(tf, "Dennis Reynolds", "Dennis is portrayed by Glenn Howerton.");
        });
        btnDee.setOnAction((ActionEvent event) -> {
            onButtonPress(tf, "Deandra Reynolds", "Dee is portrayed by Kaitlin Olson.");
        });
        btnIASIP.setOnAction((ActionEvent event) -> {
            onButtonPress(tf, "It's Always Sunny in Philadelphia", "This comedey has "
                    + "been airing since 2005 \nand it currently has 15 seasons.");
        });
         
        //Scene and Stages
        Scene scene = new Scene(pane);
        primaryStage.setTitle("It's Always Sunny in Philadelphia: Main Cast");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.setWidth(750);
        primaryStage.setHeight(600);
        primaryStage.show();
    }

    public void onButtonPress(TextField tf, String charName, String actorRole) {
        tf.setText(charName);
        System.out.println(actorRole);
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}